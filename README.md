# Python Script Assetwhere Uplift

![](https://www.omnilink.co.uk/wp-content/uploads/2018/10/EHG-RGB-e1541043422899.png)

### Assetwhere uplift instructions manual

This document is a manual to explain how to use the scrips to run into the database of any school. 
###### Pre-requisites:
- Python 3.x
- Fiona
- FME
- Connection to alobucket via VPN


#### Uplift Processing

1.	Copy gpkg to assetwhere uplift folder located in: \192.168.1.102\alobucket.omnilink.com.au\CurrentProjects\AssetWhere\AssetWhere Uplift\gpkg
 
2.	Check names of gpkgs if they follow standard for uplift to work (need to define standard naming in a “data dictionary” – could be an excel file or another word document – define them and put link here. E.g. The right name is “Building” no “Buildings” …
3.	Run the python script in cmd prompt not via Python IDLE – where is the python located:
\\192.168.1.102\alobucket.omnilink.com.au\CurrentProjects\AssetWhere\AssetWhere Uplift\python\python-git\assetwhere_uplift.py
 
4.	The script will ask you if the school is new or not with the answer options (Y or n) Be aware the “Y” is in capital letter and “n” isn’t. You must put the same character.
 
5.	Then the script will ask you What is the school’s install code? 
  You have to type the name of the school as shows in the database. 
6.	Then, the script will ask in which state are? D or DEV? Explain the meaning of D, DEV, UAT, P – however, we will be removing DEV because we used DEV for testing purposes. The script should run only on D

7.	Follow prompts on script – check for any error and any ‘unread’ files
NOTE: FME will not run after python script is completed 