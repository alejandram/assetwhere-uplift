import pyodbc, sys, os
import fiona

checknew = {'Y':True, 'n':False}

try:
    isnew = checknew[input("Is this school new? Y or n\n")]
except:
    print('Please choose either Y or n\nProgram Terminated')
    sys.exit()

school_db = input('What is the school\'s install code?\n')

# Create a small function that empties all tables if school is new

#Test to commit

# Modify so it can become user input

#'\\\\192.168.1.102\\alobucket.omnilink.com.au\\CurrentProjects\\AssetWhere\\NSW\\St Joseph College\\working\\GPKG'
#gpkg_dir = input('Please indicate location of geopackage folder\nafter the AssetWhere Folder\n')
gpkg_dir = "\\\\192.168.1.102\\alobucket.omnilink.com.au\\CurrentProjects\\Assetwhere\\AssetWhere Uplift\\gpkg"

try:
    os.chdir(gpkg_dir)
except:
    print('Location of geopackage is incorrect\nProgram Terminated')
    sys.exit()

# Define asset_type dictionary
asset_type_data = {'site': 1, 'buildings': 2, 'rooms': 3, 'service_line_data': 4, 'service_point_data': 4, 'related_asset': 5}
#asset_type = 'building'

state = {'D': "ddb0001.c7hycw79athh.ap-southeast-2.rds.amazonaws.com,1433",'DEV': "dev0001.c7hycw79athh.ap-southeast-2.rds.amazonaws.com"}

try:
    srvr = state[input("Which state is this school? D or DEV\n")] #"dev0001.c7hycw79athh.ap-southeast-2.rds.amazonaws.com" May need to add UAT or Production???
except:
    print('Please choose either D or DEV\nProgram Terminated')
    sys.exit()

usrname = input('\nPlease input the usrname: \n')
aw_pwd = input('\nPlease input the password: \n')

print(f'\nServer is: {srvr}\nDatabase is: {school_db}\nUsername is: {usrname}\nPassword is: {aw_pwd}')

def emptyTables():
    try:
        conn = pyodbc.connect(
               'DRIVER={SQL Server};'
               'SERVER='+srvr+';'
               'DATABASE='+school_db+';'
               'UID='+usrname+';'
               'PWD='+aw_pwd+';')
    except:
        print('Connection to database failed! \nPls check password...')
        sys.exit()
    #create cursor
    crsr = conn.cursor()

    #add Queries
    #crsr.execute("delete from service_component_type")
    #crsr.execute(f"DBCC CHECKIDENT (service_component_type, RESEED, 0)")
    #crsr.execute("delete from service_component_link")
    #crsr.execute(f"DBCC CHECKIDENT (service_component_link, RESEED, 0)")
    crsr.execute("delete from floors")
    crsr.execute(f"DBCC CHECKIDENT (floors, RESEED, 0)")
    crsr.execute("delete from related_assets")
    crsr.execute("delete from service_line_data")
    crsr.execute("delete from service_point_data")
    crsr.execute("delete from room")
    crsr.execute("delete from building")
    crsr.execute("delete from site")
    crsr.execute("delete from asset_register")

    print("All tables have been emptied... continuing process...")

    crsr.commit()
    crsr.close()
    conn.close()


def aw_uplift(db_name, asset_type):

    try:
        with fiona.open(f'{asset_type}.gpkg','r') as aw:
            aw_count = len(list(aw))
            aw_list = list(aw)
            aw_schema = aw.schema
            aw_crs = aw.crs
            #print(f'there are {aw_count} buildings')
    except:
        print(f'Cannot connect to {asset_type} geopackage!')
        sys.exit()

    # Define connection parameters to D state
#    srvr = "ddb0001.c7hycw79athh.ap-southeast-2.rds.amazonaws.com,1433"
# DEV Server

    #aw_pwd = str(input('Enter the password: '))

    asset_type_id = asset_type_data[asset_type]

    try:
        conn = pyodbc.connect(
               'DRIVER={SQL Server};'
               'SERVER='+srvr+';'
               'DATABASE='+db_name+';'
               'UID='+usrname+';'
               'PWD='+aw_pwd+';')
    except:
        print('Connection to database failed! \nPls check password...')
        sys.exit()

    #create cursor
    crsr = conn.cursor()

    #add Queries
    rows = crsr.execute("select * from asset_register").fetchall()

    # We don't really need to print - we can comment this out
    #for i in rows:
    #    print(i)

    #find what is the maximum value for id
    max_id = crsr.execute("select max(id) from asset_register").fetchval()
    #print(max_id)
    if not max_id:
        max_id = 0

    #print("The maximum id value in asset_register table is: " + str(max_id))

    # Reset the seed, to avoid ids being skipped
    crsr.execute(f"DBCC CHECKIDENT (asset_register, RESEED, {max_id})")

    # Insert values into SQL Server table
    # NOTE: if bulk-inserting, max value is 1000

    for i in range(aw_count):
        crsr.execute(f'insert into asset_register (asset_type, created_by, created_at, updated_by, updated_at) values \
         ({asset_type_id}, 1, SYSDATETIME(),1, SYSDATETIME())')


    # WRITE GEOPACKAGE BEGINS HERE
    j = max_id+1
    for i in range(0, aw_count):
       aw_list[i]['properties']['id'] = j
       j += 1

    #c_schema = {'geometry': 'Polygon',
    #             'properties': ([('id', 'int')])}

    #aw_crs = {'no_defs': True, 'ellps': 'WGS84', 'datum': 'WGS84', 'proj': 'longlat'}
    #aw_crs = {'init': 'epsg:4326', 'no_defs': True}
    with fiona.open(f'{asset_type}.gpkg','w', driver='GPKG', schema=aw_schema, crs=aw_crs) as aw:
        aw.writerecords(aw_list)

    crsr.commit()
    crsr.close()
    conn.close()

if isnew:
    try:
        doubleCheck = checknew[input('\n\nCAUTION! You selected New School - all data will be deleted! Continue? Y/n\n')]
    except:
        print("Please respond either Y or n\nProgram Terminated")
        sys.exit()
    if doubleCheck:
        print('\nSchool is NEW, deleting and reseeding existing data...\n')
        emptyTables()
    else:
        print('School is NOT new, continuing with existing data...')


for i in asset_type_data:
    if os.path.isfile(f'{i}.gpkg'):
        print(f'Running {i}...')
        aw_uplift(school_db,i)
    else:
        print(f'File {i}.gpkg doesn\'t exist. Continuing with next file.')
        continue

print('asset_register and geopackages IDs have been successfully updated!')

print('Running FME...')

# See there are 9 {} in the script
#yes, ok to optimised
# so this .format should be like this: format(gpkg_dir, gpkg_dir,gpkg_dir,gpkg_dir,gpkg_dir,....) 9 times
# I am asking mr. google if we can only use once
# I have my own python journal hold on
# found a possible solution, let me try - basically I am asking to look at the first element of .format(gpkg_dir) which is gpkg_dir. Ok to call it? Yes,
#Wait, I would like you to call only this. instead of running all code again
# create a new python filecopy/paste this
# copy/paste gpkg_dir
# call it fme_run.py
#
#os.chdir("\\\\192.168.1.102\\alobucket.omnilink.com.au\\CurrentProjects\\AssetWhere\\AssetWhere Uplift\\fme")
# os.chdir("Z:\\CurrentProjects\\AssetWhere\\AssetWhere Uplift\\fme")
# os.system("""fme.exe geopackage2mssql_spatial.fmw"
#               --DestDataset_MSSQL_SPATIAL_8 "{1}"
#               --SourceDataset_GEOPACKAGE_12 "{0}\\Service component type.gpkg"
#               --SourceDataset_GEOPACKAGE_10 "{0}\\Service component link.gpkg"
#               --DestDataset_MSSQL_SPATIAL_12 "{1}"
#               --FEATURE_TYPES_2 "ls_carp ls_contours ls_gate ls_landscaping ls_road ls_tree ls_bollard ls_embankment ls_fnce ls_gden ls_path ls_pond ls_steps ls_wall"
#               --DestDataset_OGCGEOPACKAGE "{0}\\room_toinvestigate.gpkg"
#               --SourceDataset_OGCGEOPACKAGE """"""{0}\\buildings.gpkg"" ""{0}\\site.gpkg""""""
#               --DestDataset_OGCGEOPACKAGE_2 "{0}\\bldg_toinvestigate.gpkg"
#               --SourceDataset_OGCGEOPACKAGE_13 "{0}\\buildings.gpkg"
#               --SourceDataset_OGCGEOPACKAGE_11 "{0}\\site.gpkg"
#               --SourceDataset_OGCGEOPACKAGE_15 "{0}\\rooms.gpkg"
#               --SourceDataset_OGCGEOPACKAGE_12 """"""{0}\\service_line_data.gpkg"" ""{0}\\service_point_data.gpkg""""""
#               --SourceDataset_OGCGEOPACKAGE_14 "{0}\\floors.gpkg"
#               --SourceDataset_OGCGEOPACKAGE_8 "{0}\\ls_*.gpkg"
#               --DestDataset_OGCGEOPACKAGE_3 "{0}\\site_toinvestigate.gpkg"
#               --DestDataset_OGCGEOPACKAGE_4 "{0}\\sld_toinvestigate.gpkg"
#               --DestDataset_OGCGEOPACKAGE_5 "{0}\\floors_toinvestigate.gpkg"
#               --DestDataset_OGCGEOPACKAGE_6 "{0}\\landscaping_toinvestigate.gpkg""".format(gpkg_dir, school_db))








#os.system("""fme.exe geopackage2mssql_spatial_v2_Testing.fmw"
#              --DestDataset_MSSQL_SPATIAL_8 "AW4_Joeys"
#              --SourceDataset_GEOPACKAGE_12 "{0}\\Service component type.gpkg"
#              --SourceDataset_GEOPACKAGE_10 "{0}\\Service component link.gpkg"
#              --DestDataset_MSSQL_SPATIAL_12 "AW4_Joeys"
#              --SourceDataset_OGCGEOPACKAGE_5 "{0}\\ls_*.gpkg"
#              --FEATURE_TYPES_2 "ls_carp ls_contours ls_court ls_gate ls_landscaping ls_pool ls_retw ls_road ls_sport ls_step ls_tree ls_fence ls_garden"
#              --DestDataset_OGCGEOPACKAGE "{0}\\To Investigate\room_toinvestigate.gpkg"
#              --SourceDataset_OGCGEOPACKAGE """"""{0}\\building.gpkg"" ""{0}\\site.gpkg""""""
#              --SourceDataset_OGCGEOPACKAGE_4 "{0}\\room.gpkg"
#              --DestDataset_OGCGEOPACKAGE_2 "{0}\\To Investigate\bldg_toinvestigate.gpkg""""".format(gpkg_dir))





#os.system("""fme.exe geopackage2mssql_spatial_v2.fmw"
#              --DestDataset_MSSQL_SPATIAL_8 "AW4_Joeys"
#              --SourceDataset_GEOPACKAGE_12 "{0}\\Service component type.gpkg"
#              --SourceDataset_GEOPACKAGE_10 "{0}\\Service component link.gpkg"
#              --DestDataset_MSSQL_SPATIAL_12 "AW4_Joeys"
#              --SourceDataset_OGCGEOPACKAGE_5 "{0}\\ls_*.gpkg"
#              --FEATURE_TYPES_2 "ls_carp ls_contours ls_court ls_gate ls_landscaping ls_pool ls_retw ls_road ls_sport ls_step ls_tree ls_fence ls_garden"
#              --SourceDataset_OGCGEOPACKAGE_4 """"""{0}\\building.gpkg"" ""{0}\\floors.gpkg"" ""{0}\\room.gpkg"" ""{0}\\service_line.gpkg"" ""{0}\\service_point.gpkg"" ""{0}\\site.gpkg""""".format(gpkg_dir))

#os.system("""fme.exe geopackage2mssql_spatial_v2.fmw"
#              --SourceDataset_GEOPACKAGE "\\\\192.168.1.102\\alobucket.omnilink.com.au\\CurrentProjects\\AssetWhere\\NSW\\St Joseph College\\Working\\GPKG\\site.gpkg"
#              --SourceDataset_GEOPACKAGE_13 "\\\\192.168.1.102\\alobucket.omnilink.com.au\\CurrentProjects\\AssetWhere\\NSW\\St Joseph College\\Working\\GPKG\\building.gpkg"
#              --SourceDataset_GEOPACKAGE_9 "\\\\192.168.1.102\\alobucket.omnilink.com.au\\CurrentProjects\\AssetWhere\\NSW\\St Joseph College\\Working\\GPKG\\room.gpkg"
#              --SourceDataset_GEOPACKAGE_11 "\\\\192.168.1.102\\alobucket.omnilink.com.au\\CurrentProjects\\AssetWhere\\NSW\\St Joseph College\\Working\\GPKG\\service_line.gpkg"
#              --SourceDataset_GEOPACKAGE_15 "\\\\192.168.1.102\\alobucket.omnilink.com.au\\CurrentProjects\\AssetWhere\\NSW\\St Joseph College\\Working\\GPKG\\service_point.gpkg"
#              --DestDataset_MSSQL_SPATIAL_8 "AW4_Joeys"
#              --SourceDataset_GEOPACKAGE_12 "\\\\192.168.1.102\\alobucket.omnilink.com.au\\CurrentProjects\\AssetWhere\\NSW\\St Joseph College\\Working\\GPKG\\Service component type.gpkg"
#              --SourceDataset_GEOPACKAGE_10 "\\\\192.168.1.102\\alobucket.omnilink.com.au\\CurrentProjects\\AssetWhere\\NSW\\St Joseph College\\Working\\GPKG\\Service component link.gpkg"
#              --DestDataset_MSSQL_SPATIAL_12 "AW4_Joeys"
#              --SourceDataset_OGCGEOPACKAGE "\\\\192.168.1.102\\alobucket.omnilink.com.au\\CurrentProjects\\AssetWhere\\NSW\\St Joseph College\\Working\\GPKG\\floors.gpkg"
#              --SourceDataset_OGCGEOPACKAGE_2 "\\\\192.168.1.102\\alobucket.omnilink.com.au\\CurrentProjects\\AssetWhere\\NSW\\St Joseph College\\Working\\GPKG\\ls_*.gpkg"
#              --FEATURE_TYPES_2 "ls_carp ls_contours ls_court ls_gate ls_landscaping ls_pool ls_retw ls_road ls_sport ls_step ls_tree ls_fence ls_garden""")
